<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wolfpt_admin');

/** MySQL database username */
define('DB_USER', 'wolfpt_admin');

/** MySQL database password */
define('DB_PASSWORD', 'LKAyKVUtT8');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2*P9Y!NY}e[HrD%0y,)axJx*9@b&%LZsg)>)R*%624?LJ`_>nkX8,rZl<^v(zv^=');
define('SECURE_AUTH_KEY',  '|1Hy<}u:Rd-]n=WM55*!5hv&=*h-2TpfMHnx(gbFKWbRFn`@wVsv6oaK:*QIJ`/}');
define('LOGGED_IN_KEY',    'x5(}(/6x!;[g[rjsTzyM(v/;#2R{bppf|$puB6Ujhm@(7$V&1ns$G?+?Q2wxfsMg');
define('NONCE_KEY',        'Pp)7V%L1n6-7sXeU(y?eJ$V:*l%(cZD!Yi0}hDq9c|clu>QM/@wghgMk,;eqp*{L');
define('AUTH_SALT',        '+67O..^1)#^s=|f&Y4ETJj~Mxr5!XD[Gbj$3 e/#urpW_i[5LDdrO<+z@Xu!lT<Q');
define('SECURE_AUTH_SALT', 'jEh.qnVoltjy}#n$>*>_0_]!e`/:l*K|t{dY$=GV%_~hxtmzHe!1yq81ixk+tn%z');
define('LOGGED_IN_SALT',   '{Am1yhBA<|da7 0(*s08:wX!FHh7(=:LQ<jj%[nBuMaxc*FQbdfvo(fwU}RM[a2 ');
define('NONCE_SALT',       '#&{flIw2.7_dUCIxbDRgnUl^x_{ER@Tq7|r}F;fVxL{EuUy+r]wWEgmV>)tHS`WM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
