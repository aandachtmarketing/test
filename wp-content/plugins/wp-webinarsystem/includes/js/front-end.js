function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

/*Custom register/login tabs*/
/* global wbnId, wpws_ajaxurl, admin */

jQuery(document).on('click', '#custom-tabs li a', function ()
{
    /*Remove active*/
    jQuery('#custom-tabs li').removeClass('active');
    jQuery('.content-wraper .tab-content').addClass('hide');

    /*add active*/
    jQuery(this).parent('li').addClass('active');
    jQuery('.content-wraper .tab-content:eq(' + jQuery(this).parent('li').index() + ')').removeClass('hide');
});

/*
 * 
 * Header bar
 * 
 */
jQuery(document).ready(function ($) {
    var loadedQ = 0;
    $('#wswebinar_private_que').hide().show().hide();
    $('#wswebinar_open_msg_cntr').click(function (e) {
        $(this).toggleClass('message-center-active').removeClass('message-center-newmsg');
        $('#wswebinar_private_que').toggle();
        e.stopPropagation();
    });
    $('#wswebinar_private_que').mousemove(function () {
        $('#wswebinar_open_msg_cntr').removeClass('message-center-newmsg');
    });
    $(document).click(function (e) {
        if (!$(e.target).is('#wswebinar_private_que, #wswebinar_private_que *')) {
            $('#wswebinar_open_msg_cntr').removeClass('message-center-active')
            $("#wswebinar_private_que").hide();
        }
    });
    
    //    Remove Monarch plugin Styles
        jQuery("[class^='et_']").hide();
});

jQuery(document).ready(function() {	
    var showstate = readCookie('showstate');
        if(showstate != 'shown')
          {
          jQuery("#admin-pro-notice").css("display","block");
          }
         else
          {
          jQuery("#admin-pro-notice").css("display","none");
          }
		jQuery('#admin-pro-notice .close').click(function(e){
		    jQuery("#admin-pro-notice").fadeOut();
		    createCookie('showstate', 'shown');
	    });
});